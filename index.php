<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>bookmark</title>
    <link rel="stylesheet" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=PT+Serif:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
</head>
<body>
        <header>
        <h1><?php echo "LIENS FAVORIS (BOOKMARKS)" ?></h1>
        <nav>
    <ul>
        <li> <?php echo "<a href='\bookmark/formulaire.php'>formulaire </a>"; ?> </li> 
        <li> <?php echo "<a href='\bookmark/formPreRempli.php'>Modifier éléments favori </a>"; ?> </li>
       
     </ul>  
</nav>
    
        </header>
    <section>
  <?php 

  /* établit connexion bdd*/
  /*def chaîne connexion (DSN)*/
  try
    {
      $bdd = new PDO('mysql:host=localhost;dbname=bookmark;charset=utf8', 'root', '');
    }
    
    catch (Exception $e)
    {
        die(' Erreur : ' .$e->getMessage());
    }

    $reponse = $bdd->query("SELECT p.id as 'id favori', c.nom as 'nom categories', p.url as 'url favori', p.nom as 'nom favori', p.date_creation as 'date creation'
    from favori as p 
    inner join groupe as g on p.id = g.id_favori   
    inner join categories as c on c.id = g.id_categories
    ");

    while ($donnees = $reponse->fetch())
    {
    ?>
        <div class="card">
            <p>
            <h2>catégories : <?php echo $donnees['nom categories']?></h2>

            <p>id favori : <?php echo $donnees ['id favori'] ?></p>
            <p>lien Favoris : <?php echo $donnees['url favori'] ?></p>
            <p>nom favoris : <?php echo $donnees['nom favori']?></p>
            <p>date création : <?php echo $donnees['date creation']?></p>
            </p>  
        </div>
             
        <?php 
    }
?>        
</section>

    
</body>
</html>