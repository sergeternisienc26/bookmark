<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire bookmark</title>
    <link rel="stylesheet" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=PT+Serif:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
</head>

<body>
    <header>
<h1><?php echo "Formulaire liens Bookmark" ?></h1>

<nav>
    <ul>
        <li><?php echo "<a href='\bookmark/index.php'>Accueil</a>"; ?></li> 
        <li> <?php echo "<a href='\bookmark/formPreRempli.php'>Modifier éléments favori</a>"; ?>
     </ul>
   
</nav>

</header>

<section>

<?php 
    try
        {
         $bdd = new PDO('mysql:host=localhost;dbname=bookmark;charset=utf8', 'root', '');
        }
    catch (Exception $e)
        {
         die(' Erreur : ' .$e->getMessage());
        }

?> 
         
<div class="formulaire">
    <h2>Insert ton lien</h2>
        <form name="insert_lien" method="post" action="formulaire.php">
            Entre nom favori : <input type="texte" name="nom_favori" placeholder="nom favori"/> </br>
            url : <input type="texte" name="url" placeholder="url"/> </br>
            date : <input type="date" name="date_creation" placeholder="AAAA-MM-JJ"/> </br>
            <input type="submit" name="valider" value="ok"/>
        </form>
</div>

<?php if (isset ($_POST['valider']))
    {
        $nom_favori=$_POST['nom_favori'];
        $url=$_POST['url'];
        $date_creation=$_POST['date_creation'];
   
    

    $reponse = $bdd->prepare("INSERT INTO `favori`(`id`, `nom`, `url`, `date_creation`)
    VALUES ('', :nom_favori, :url, NOW())");
    $reponse->bindValue(':nom_favori', $nom_favori, PDO::PARAM_STR);
    $reponse->bindValue(':url', $url, PDO::PARAM_STR);
    
    $reponse->execute();
     
    $reponse->closeCursor();
    
    }

?>
        
</section>


</body>
</html>